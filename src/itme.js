export default [
    {
      img: "https://tocode.ru/static/uploads/courses/3/clash-of-clans-images/barbarian.png",
      title:"The Barbarian",
      text:"The Barbarian is a kilt-clad Scottish warrior with an angry, battle-ready expression, hungry for destruction. He has Killer yellow horseshoe mustache.",
      name:"barbarian"
    },
    {
      img: "https://tocode.ru/static/uploads/courses/3/clash-of-clans-images/archer.png",
      title:"The archer",
      text:"The Archer is a female warrior with sharp eyes. She wears a short, light green dress, a hooded cape, a leather belt and an attached small pouch.",
      name:"archer"
    },
    {
      img: "https://tocode.ru/static/uploads/courses/3/clash-of-clans-images/giant.png",
      title:"The giant",
      text: "Slow, steady and powerful, Giants are massive warriors that soak up huge amounts of damage. Show them a turret or cannon and you will see their fury unleashed!",
      name:"giant"
    },
    {
      img: "https://tocode.ru/static/uploads/courses/3/clash-of-clans-images/goblin.png",
      title:"The goblin",
      text:"Slow, steady and powerful, Giants are massive warriors that soak up huge amounts of damage. Show them a turret or cannon and you will see their fury unleashed!",
      name:"goblin"
    },
    {
      img: "https://tocode.ru/static/uploads/courses/3/clash-of-clans-images/wizard.png",
      title:"The Wizard",
      text:"The Wizard is a terrifying presence on the battlefield. Pair him up with some of his fellows and cast concentrated blasts of destruction on anything, land or sky!",
      name:"Wizard"
    },
    {
      img: "https://tocode.ru/static/uploads/courses/3/clash-of-clans-images/barbarian.png",
      title:"The Barbarian",
      text:"The Barbarian is a kilt-clad Scottish warrior with an angry, battle-ready expression, hungry for destruction. He has Killer yellow horseshoe mustache.",
      name:"Barbarian"
    },
    {
      img: "https://tocode.ru/static/uploads/courses/3/clash-of-clans-images/archer.png",
      title:"Main title",
      text:"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
      name:"alias2"
    },
    {
      img: "https://tocode.ru/static/uploads/courses/3/clash-of-clans-images/giant.png",
      title:"Main title",
      text: "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.",
      name:"alias3"
    },
  //  ,
    // {
    //   img: "https://picsum.photos/200/300?grayscale",
    //   title:"Main title",
    //   text: "somthing text here.............",
    //   name:"alias6"
    // },
    // {
    //   img: "https://picsum.photos/id/870/200/300?grayscale&blur=2",
    //   title:"Main title",
    //   text:" It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
    //   name:"alias7"
    // },
    // {
    //   img: "https://picsum.photos/200/300?grayscale",
    //   title:"Main title",
    //   text:"Some quick example text to build on the card title and make up the bulk of the card's content.",
    //   name:"alias8"
    // },
    // {
    //   img: "https://picsum.photos/seed/picsum/200/300",
    //   title:"Main title",
    //   text:"The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. ",
    //   name:"alias9"
    // },
    // {
    //   img: "https://picsum.photos/id/237/200/300",
    //   title:"Main title",
    //   text:"It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.",
    //   name:"alias10"
    // },
    // {
    //   img: "https://picsum.photos/200/300?image=0",
    //   title:"Main title",
    //   text:"The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. ",
    //   name:"alias11"
    // },
    // {
    //   img: "https://picsum.photos/200/300",
    //   title:"Main title",
    //   text:"Some quick example text to build on the card title and make up the bulk of the card's content.",
    //   name:"alias12"
    // },
  ]